import json
from django.conf import settings
from django.http import HttpResponse
from django.shortcuts import render
from django.shortcuts import render_to_response
from django.contrib.auth import get_user_model
from django.core.paginator import Paginator
from django.template.context import RequestContext

from social.apps.django_app.default.models import UserSocialAuth
from social.apps.django_app.utils import load_strategy
from models import Track
from scraping import save_track_info

import tasks
import requests
import json
import soundcloud
import base64
import re
import urlparse
# Create your views here.

import logging
logger = logging.getLogger(__name__)

def home(request):
	tracks = []
	soundcloud_auth = False
	pnos=0
	next_page = 0
	prev_page = 0
	current_page = request.GET.get('current_page',1)
	current_page = int(current_page)

	logger.debug('current_page is %s' % current_page)

	if request.user and not request.user.is_anonymous() and not request.user.is_superuser:
	#if request.user and not request.user.is_anonymous():
		try:
			soundcloud_auth = UserSocialAuth.objects.get(user=request.user,provider="soundcloud")

			if soundcloud_auth:
				soundcloud_auth = True
		except Exception, e:
			#Nothing to worry , Sound cloud isn't connected
			print e

		#Try refreshing the token
		if soundcloud_auth:
			try:
				User = get_user_model()
				suser = User.objects.get(id=request.user.id)
				social = suser.social_auth.get(provider='soundcloud')
				social.refresh_token(load_strategy())
			except Exception, e:
				logger.error(e)

	tracks = Track.objects.filter(user_id=request.user.id).order_by('-pk')

	p = Paginator(tracks,10)
	pnos = p.num_pages
	if current_page > p.num_pages:
		current_page = p.num_pages
	page1 = p.page(current_page)
	tracks = page1.object_list
	next_page = current_page + 1
	if next_page > p.num_pages:
		next_page = 0

	prev_page = current_page - 1
	if prev_page < 0:
		prev_page = 0

	context = RequestContext(request, {
			'user': request.user,
			'soundcloud':soundcloud_auth,
			'tracks':tracks,
			'pnos':pnos,
			'current_page':current_page,
			'next_page':next_page,
			'prev_page':prev_page})

	return render_to_response('home.html',context_instance=context)

def get_page(request):
	tracks = []
	page_no = request.GET.get('page')
	if request.user.is_authenticated():
		p = Paginator(Track.objects.filter(user_id=request.user.id), 10)
		pnos = p.num_pages
		page1 = p.page(page_no)
		tracks = page1.object_list
	return tracks

def sync(request):
	domain = request.META['HTTP_HOST']
	logger.info('Sync - %s - %ssecure' % (domain, '' if request.is_secure() else 'NOT '))

	protocol = 'https' if request.is_secure() else 'http'
	domain = protocol + '://' + domain

	response = {}
	if not request.user.is_authenticated():
		response['error'] = 'No user is authenticated'
	else:
		google = UserSocialAuth.objects.filter(user=request.user,provider="google-oauth2")
		google = google.first() if google.exists() else None
		if not google:
			message = 'No google auth for %s' % request.user
			response['error'] = message
			logger.warn(message)
		else:
			start = request.GET.get('from')
			end = request.GET.get('end')
			tasks.scrape.delay(google.pk, start, end)
	return HttpResponse(json.dumps(response))

def save_track_info(request):
	link = request.GET.get('link')
	thumbnail = request.GET.get('thumbnail_url')
	title = request.GET.get('title')
	author = request.GET.get('author')
	author_url = request.GET.get('author_url')
	embed = request.GET.get('embed')
	track_type = request.GET.get('track_type')
	save_track_info(request.user, link, embed, title, author, author_url, thumbnail, track_type)

def follow_user(request):
	try:
		url = request.GET.get('url')
		SOUNDCLOUD_KEY = settings.SOCIAL_AUTH_SOUNDCLOUD_KEY
		client = soundcloud.Client(client_id=SOUNDCLOUD_KEY)
		soundcloud_user = client.get('/resolve', url=url)
		if request.user and request.user.is_anonymous() is False and request.user.is_superuser is False:
			soundcloud_data = UserSocialAuth.objects.get(user=request.user,provider="soundcloud")
			if soundcloud_data:
				client = soundcloud.Client(access_token=soundcloud_data.extra_data['access_token'])
				response = client.put('/me/followings/'+str(soundcloud_user.id))
				print json.loads(response.text)
	except Exception, e:
		print e

	return render_to_response('sync.html')

def like_track(request):
	try:
		url = request.GET.get('url')
		SOUNDCLOUD_KEY = settings.SOCIAL_AUTH_SOUNDCLOUD_KEY
		client = soundcloud.Client(client_id=SOUNDCLOUD_KEY)
		track = client.get('/resolve', url=url)
		if request.user and request.user.is_anonymous() is False and request.user.is_superuser is False:
			soundcloud_data = UserSocialAuth.objects.get(user=request.user,provider="soundcloud")
			if soundcloud_data:
				client = soundcloud.Client(access_token=soundcloud_data.extra_data['access_token'])
				response = client.put('/me/favorites/'+str(track.id))
				print json.loads(response.text)
	except Exception, e:
		print e

	return render_to_response('sync.html')
